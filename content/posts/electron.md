---
title: Electron
date: 2021-04-25T22:11:24+02:00
draft: false
language: en
tags:
  - electron
  - rant
---

Our favourite hot pile of steaming garbage ❤. I'm not really sure how it is that nobody has made
something more sane than that thing. I respect the work of those developer that still work their
asses of to make it functional an try to make it safer and safer day in and day out. The thing I
don't understand why is it that it got so bloated.

Rule number one if you want to be able to maintain a project with minimal bugs: keep it simple.
Electron is great in it's inovation and how they made it possible for meer mortals to write a web
app and put it on a desktop. Right now it's getting harder and harder to use it. I've just had to
deal with Spectron, a testing tool for Electron apps. Basically WebDriver and ChromeDriver along
side a nice api for getting data from renderer process set up for Electron so you don't have to deal
with that messy stuff. It's basically not working.

I know how it feels to try and maintain a project that is buggy and bloated and to get yelled at by
some douche online. It's not great, but at least acknowledge that it's buggy and bloated and tell
everyone something better should be made. Something minimal and easily extensible so it would
actually be maintainable.

Mainly the issue I have with Electron is the poor documentation and a lot of unexpected behavior
that is either poorely documented or there is documentation telling otherwise. The bugs come with
the scale of the project. It would be somewhat manageable if there was some other tactic for
packaging applications than "Let's funcking statically link everything!". Arch Linux is taking a
good aproach of providing Electron as a package and all Electron applications actually run using the
same Electron package so you wouldn't have a copy of Chromium and Node.js for each app you have.

There are some solutions out there that are trying to make an Electron alternative that would be as
funcional as Electron but more lightweight. [Tauri](https://github.com/tauri-apps/tauri) takes the
aproach of using native rendering engine in order to display the application. It won't be the latest
and greates of the web tech that you would get on Chromium but it's a fine thing if you're looking
for something simple and lightweight. [Lorca](https://github.com/zserge/lorca) on the other hand
uses Chrome/Chromium but you are required to use the already installed version. It might be a bit
troublesome to distribute applications that way on Windows or macOS where you can't really specify
Chrome/Chromium as a dependancy of an application bu but in my opinion, they're both onto something
here. Both options do not currently replace Electron fully but I think that there IS a future
without 500 MB desktop applications.

On the final note, Electron is a fine piece of engineering but i think that its future is getting
darker and darker and it's just the matter of time until shit hits the fan and exploits and bugs
start poping up all over the place making it effectively unusable. It's not constructive to always
rant about bad things but right now I think people should think about what problems could this make
and how it can be fixed.
