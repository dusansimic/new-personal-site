---
title: Fish shell abbreviations
date: 2022-07-28
language: en
tags:
  - linux
  - config
---

I've been using Fish shell for quite a few years and I've been pretty happy with what communty themes and configs had to offer. From time to time I added a short line to the `config.fish` file but only lately have I gotten into configuring the shell a bit more.

Most of the configs are just some of my preferences but the Fish command I enjoyed the most is `abbr` (creating abbreviations for commands). For some of you a bulb might have lit up and you've said, "That's kind of like `alias`.", and you're right but not completely.

`abbr` does create an alias for a command but instead of just being an alias, it expands the alias to it's full command. For detailed docs, open a Fish session and type `abbr --help` or `man abbr`.

A simple cheatsheet is here for those that are lazy. `-a` option adds a new command and `-g` option means it's added globally.

```sh
$ abbr -ag gc 'git commit'
$ abbr -ag gp 'git push'
```

In order to persist these settings, add them to your `config.fish`
