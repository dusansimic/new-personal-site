---
title: About me
type: info
date: 2022-07-28
---

I'm a Computer Science student at Faculty of Sciences, University of Novi Sad, Serbia. In my free time, I like to work on open source software and organize community events, workshops and lectures. My main interests are full stack app development, systems administration (Linux) and creating tools that will improve developers and peoples experience.

## Stuff I use

* __OS__ Arch Linux (desktop), Fedora Workstation (laptop), Rocky Linux (servers)
* __Desktop__ Gnome
* __Editors__ VSCode, NeoVim (for command line stuff)
* __Shell__ Fish, [my own theme](https://github.com/dusansimic/theme-nai)
* __Browser__ Firefox, Chromium (only if I have to)
* __Email__ Thunderbird, previously Geary but it's a bit buggy now and still doesn't have encryption support
* __Languages and tools__ Go, TypeScript, Python, POSIX/Bash/Fish shell and a lot of other stuff
* __Selfhosting__ Miniflux, Gitea, Vaultwarden
* __Social__
[Instagram](https://instagram.com/dusansimic)
[Twitter](https://twitter.com/dsnsmc)
[Mastodon](https://mastodon.technology/dusansimic)
[Pixelfed](https://pixelfed.social/dusansimic)
